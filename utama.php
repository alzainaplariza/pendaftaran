<?php 

session_start();

if( !isset($_SESSION["login"])) {
  header("Location: login.php");
  exit;
}

require 'functions.php';
$registrasi = query("SELECT * FROM registrasi");
// mengambil data barang
$data_peserta = mysqli_query($conn,"SELECT * FROM registrasi");
 
// menghitung data barang
$jumlah_pendaftar = mysqli_num_rows($data_peserta);
?>
 

<!DOCTYPE html>
<html lang="en">
<head>
<title>Website Pendaftaran Ekskul</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

body {
  margin: 0;
}

/* Style the header */
.header {
  background-color: #f1f1f1;
  padding: 20px;
  text-align: center;
}
ul  {
 font-family: "Lucida Console", "Courier New", monospace;
 list-style-type: none;

}

ul li{
 
  color: black;

}
ul li{
 
  font-weight: bold;
  margin-right:50px;
  padding:  10px 100px 30px 0px;
  text-align: center;

}
ul li a{
  text-decoration: none;
  color:black;

}


/* Style the top navigation bar */
.topnav {
  overflow: hidden;
  background-color: #696969;
}

/* Style the topnav links */
.topnav a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

/* Change color on hover */
.topnav a:hover {
  background-color: #ddd;
  color: black;
}

/* Create three unequal columns that floats next to each other */
.column {
  float: left;
  padding: 10px;
}

/* Left and right column */
.column.side {
  width: 250px;
  height: 700px;
  background-color: #A9A9A9;
}

/* Middle column */
.column.middle {
  width: 50%;
  margin-left:60px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}


/* Responsive layout - makes the three columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column.side, .column.middle {
    width: 100%;
  }
}

/* Style the footer */
.footer {
  background-color: #696969;
  padding: 10px;
  text-align: center;
}
</style>
</head>
<body>

<div class="header">
<h1>Pendaftaran Ekstrakurikuler MAKN ENDE</h1>
<p>Belajarlah untuk meningkatkan keterampilan anda!!</p>
</div>

<div class="topnav">
<marquee><a href="#">Madrasah Aliyah Kejuruan Negeri Ende    ---"Pendidikan adalah kemampuan untuk mendengarkan segala sesuatu tanpa membuatmu kehilangan temperamen atau rasa percaya-dirimu". - Robert Frost---</a></marquee>
</div>

<div class="row">
<div class="column side">
<h2>DASHBOARD</h2>
<menu>
		<ul>
			<li><a href="utama.php">Home</a></li>
			<li><a href="registration2.php">Register</a></li>
			<li><a href="userr.php">user</a></li>
      <li><a href="login.php">login</a></li>
      <li><a href="logout.php">Logout</a></li>
		</ul>
	</menu>
</div>

<div class="column middle">
<p>Jumlah pendaftar : <b><?php echo $jumlah_pendaftar; ?></b></p>
<h2>Daftar Nama Peserta Ekskul</h2>
<a href="tambah.php">pendaftaran Ekskul Makn Ende</a></a>
<br><br>

<table border="1" cellpadding="10" cellspacing="0">

<tr>
    <th>No.</th>
    <th>Aksi</th>
    <th>Nama</th>
    <th>Kelas</th>
    <th>TTL</th>
    <th>Email</th>
    <th>Nama Ekskul</th>
</tr>
<?php $i =1; ?>
<?php foreach ($registrasi as $row):?>
<tr>
    <td><?= $i;?></td>
   <td>
    <a href="ubah.php?id=<?= $row ["id"]; ?>">Ubah</a> |
    <a href="hapus.php?id=<?= $row ["id"]; ?>" onclick="return confirm('yakin? mau dihapus');">Hapus</a>
   </td>
   <td><?= $row["nama"];?></td>
   <td><?= $row["kelas"];?></td>
   <td><?= $row["tgl_lahir"];?></td>
   <td><?= $row["email"];?></td>
   <td><?= $row["jenis_ekskul"];?></td>
</tr>
<?php $i++; ?>
<?php endforeach; ?>

</table>
</div>


</div>

<div class="footer">
<p>by: S. Fatmawati MH          |         XI RPL 1</p>
</div>

</body>
</html>