-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 16 Jun 2023 pada 08.30
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pendaftaran`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `registrasi`
--

CREATE TABLE `registrasi` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kelas` varchar(50) NOT NULL,
  `tgl_lahir` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `jenis_ekskul` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `registrasi`
--

INSERT INTO `registrasi` (`id`, `nama`, `kelas`, `tgl_lahir`, `email`, `jenis_ekskul`) VALUES
(7, 'Arum Nayla farhani', 'XI RPL 1', 'maumere,10 januari 2003', 'nayfar14@gmail.com', 'Volly'),
(9, 'Fatmawati', 'XI RPL  1', 'Malaysia, 15 Agustus 2005', 'sfatmaw15@gmail.com', 'Paskibraka'),
(13, 'Ulum Tahirah', 'XI RPL 2', 'maumere,10 januari 2003', 'ulumtahirahxirpl@gmail.com', 'Paskibraka');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `level`) VALUES
(14, 'fatmawatimh', 'sfatmaw15@gmail.com', '$2y$10$LjcRXzOdORSE8XltpU7BGui4Q0KIqgkzhRYZeAjW1SmAwdGVvTLnu', 'admin'),
(15, 'ulum tahirah', 'ulumtahirahxirpl@gmail.com', '$2y$10$Xmwtvf0l1REx/TQCbYtCFutxc3vp2aN2UYikmtuy297RsUkEnafk6', 'anggota'),
(16, 'neneng21', 'fidatamziel@gmail.com', '$2y$10$AYpbqHYdNpBuIQLdEhyWzOMUUcLCYolVjr5C4jcDj9LhDO1TfUIJC', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `registrasi`
--
ALTER TABLE `registrasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `registrasi`
--
ALTER TABLE `registrasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
