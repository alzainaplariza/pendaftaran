<?php 

session_start();

if( !isset($_SESSION["login"])) {
  header("Location: login.php");
  exit;
}

require 'functions.php';
$registrasi = query("SELECT * FROM registrasi");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Website Pendaftaran Ekskul</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

body {
  margin: 0;
}

/* Style the header */
.header {
  background-color: #f1f1f1;
  padding: 20px;
  text-align: center;
}
ul  {
 font-family: "Lucida Console", "Courier New", monospace;
 list-style-type: none;

}

ul li{
 
  color: white;

}
ul li{
 
  font-weight: bold;
  margin-right:50px;
  padding:  10px 100px 30px 0px;
  text-align: center;

}
ul li a{
  text-decoration: none;
  color:white;

}


/* Style the top navigation bar */
.topnav {
  overflow: hidden;
  background-color: #696969;
}

/* Style the topnav links */
.topnav a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

/* Change color on hover */
.topnav a:hover {
  background-color: #ddd;
  color: black;
}

/* Create three unequal columns that floats next to each other */
.column {
  float: left;
  padding: 10px;
}

/* Left and right column */
.column.side {
  width: 250px;
  height: 700px;
  background-color: #A9A9A9;
}

/* Middle column */
.column.middle {
  width: 50%;
  margin-left:60px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}


/* Responsive layout - makes the three columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column.side, .column.middle {
    width: 100%;
  }
}

/* Style the footer */
.footer {
  background-color: #f1f1f1;
  padding: 10px;
  text-align: center;
}
</style>
</head>
<body>

<div class="header">
<h1>Pendaftaran Ekstrakurikuler MAKN ENDE</h1>
<p>Belajarlah untuk meningkatkan keterampilan anda!!</p>
</div>

<div class="topnav">
<marquee><a href="#">Madrasah Aliyah Kejuruan Negeri Ende    ---"Pendidikan adalah kemampuan untuk mendengarkan segala sesuatu tanpa membuatmu kehilangan temperamen atau rasa percaya-dirimu". - Robert Frost---</a></marquee>
</div>

<div class="row">
<div class="column side">
<h2>DASHBOARD</h2>
<menu>
		<ul>
			<li><a href="utama.php">Home</a></li>
			<li><a href="profile.php">Profile</a></li>
			<li><a href="registration2.php">Register</a></li>
			<li><a href="userr.php">user</a></li>
			<li><a href="contact">Contact</a></li>
      <li><a href="login.php">login</a></li>
      <li><a href="logout.php">Logout</a></li>
		</ul>
	</menu>
</div>
<main>
    
</main>


<div class="footer">
<p>Footer</p>
</div>

</body>
</html>