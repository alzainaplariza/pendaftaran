<?php

session_start();

if( isset($_SESSION["login"])) {
	header("Location: utama.php");
	exit;
}

require 'functions.php';

if ( isset ($_POST["login"]) ) {

    $username = $_POST["username"];
    $password = $_POST["password"];

    $result = mysqli_query($conn, "SELECT * FROM user WHERE username = '$username'");

    if(mysqli_num_rows($result) === 1 ) {


        $row = mysqli_fetch_assoc($result);
       if( password_verify($password, $row["password"]) ) {
		//set_session
			$_SESSION["login"] = true;
            header("Location: utama.php");
            exit;
       }
    }
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Halaman login</title>
	<link rel="stylesheet" type="text/css" href="style3.css">
</head>
<body>
 
	
 
	<div class="kotak_login">
		<p class="tulisan_login">Silahkan Login</p>
		<form action="" method="post">
		
				<label for= "username">Username :</label>
				<input type="text" name="username" class="form_login" id="username" required>
			
			
				<label for= "password">Password :</label>
				<input type="password" name="password" class="form_login" id="password" required>
			
			    <button type="submit" class="tombol_login" name="login" value="LOGIN">Login</button>
			
			<br/>
			<br/>
		
			<center>
				<a class="link" href="utama.php">kembali</a>
			</center>
		</form>
		
	</div>
 
 
</body>
</html>