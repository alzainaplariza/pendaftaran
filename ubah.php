<?php

session_start();

if( !isset($_SESSION["login"])) {
  header("Location: login.php");
  exit;
}

require 'functions.php';

//ambil data url

$id = $_GET["id"];

$pdf = query("SELECT * FROM registrasi WHERE id = $id")[0];



if (isset ($_POST["submit"]) ) {

    if(ubah($_POST) > 0 ) {
        echo "
        <script>
    alert ('data berhasil diubah!');
    document.location.href = 'utama.php';
    </script>
    ";

    }else {
        echo "
        <script>
    alert ('data gagal diubah!');
    document.location.href = 'registration2.php';
    </script>
    ";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>update data siswa</title>
</head>
<body>
    <h1>Update data siswa</h1>
    <form action="" method="post">
        <input type="hidden" name="id" value="<?= $pdf["id"];?>">
        <ul>
        <ul>
            <li>
                <label for="nama">Nama</label>
                <input type="text" name="nama" id="nama" value="<?= $pdf["nama"];?>"
                require>
            </li>
            <li>
                <label for="kelas">Kelas</label>
                <input type="text" name="kelas" id="kelas" value="<?= $pdf["kelas"];?>">
            </li>
            <li>
                <label for="tgl_lahir">TTL</label>
                <input type="text" name="tgl_lahir" id="tgl_lahir" value="<?= $pdf["tgl_lahir"];?>">
            </li>
            <li>
                <label for="email">Email</label>
                <input type="text" name="email" id="email" value="<?= $pdf["email"];?>">
            </li>
            <li>
                <label for="jenis_ekskul">Nama Ekskul</label>
                <input type="text" name="jenis_ekskul" id="jenis_ekskul" value="<?= $pdf["jenis_ekskul"];?>">
            </li>
        
            <li>
                <button type=submit name="submit">Update data!</button>
            </li>
        </ul>
    </form>
</body>
</html>