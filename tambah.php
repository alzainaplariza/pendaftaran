<?php

session_start();

if( !isset($_SESSION["login"])) {
  header("Location: login.php");
  exit;
}

require 'functions.php';

if (isset ($_POST["submit"]) ) {

    if( tambah($_POST) > 0 ) {
        echo "
        <script>
    alert ('data berhasil ditambahkan!');
    document.location.href = 'utama.php';
    </script>
    ";

    }else {
        echo "
        <script>
    alert ('data gagal ditambahkan!');
    document.location.href = 'utama.php';
    </script>
    ";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tambah data mahasiswa</title>
    <link rel="stylesheet" type="text/css" href="style3.css">
    <style>
        h3{
            text-align: center;
            padding-bottom: -30px;
        }
    </style>
</head>
<body>
    <h1>PENDAFTARAN EKSTRAKURIKULER MAKN ENDE</h1>
    <div class="kotak_login">
		<p class="tulisan_login">Tambahkan</p>
		<form action="" method="post">
        
            
                <p><label for="nama">Nama</label>
                <input type="text" name="nama" id="nama"
                require></p>
            
            
                <p><label for="kelas">Kelas</label>
                <input type="text" name="kelas" id="kelas"></p>
            
            
                <p><label for="tgl_lahir">TTL</label>
                <input type="text" name="tgl_lahir" id="tgl_lahir"></p>
            
            
                <p><label for="email">Email</label>
                <input type="text" name="email" id="email"></p>
            
           
            
            <p><label>Ekskul:</label>
                <input type="radio" name="jenis_ekskul" id="jenis_ekskul" value="Robotik"> Robotik
                <input type="radio" name="jenis_ekskul" id="jenis_ekskul" value="Paskibraka"> Paskibraka
                <input type="radio" name="jenis_ekskul" id="jenis_ekskul" value="Volly"> Volly</p>
            
                <p> <button type="submit" name="submit"  >Tambahkan</button></p>
                
            
        
    </form>
    </div>
</body>
</html>